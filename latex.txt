\documentclass[a4paper,17pt]{beamer}
\usetheme{Madrid}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\title{\textbf{Puzzle}}
\author{Team 30}
\begin{document}
\begin{frame}
\maketitle
\begin{figure}[htp]
    \centering
    \includegraphics[width=4cm,height =80]{grid.png}
    \caption{\textbf{Number Puzzle}}
    \label{fig:galaxy}
\end{figure}
\end{frame}
\begin{frame}{Team Members}
\begin{itemize}
  \item 20B01A0547-G.Sai Sowmya
  \item 20B01A0335-N.Prajwala Chowdary
  \item 20B01A0453-G.Amrutha Varshini
  \item 21B05A0502-B.Dhakshayini
  \item 20B01A0334-M.Juhi sree ramya
\end{itemize}
   
\end{frame}


\begin{frame}{Data Flow Layout Diagram}
\begin{figure}[htp]
    \centering
    \includegraphics[width=10cm]{data.png}
    \caption{\textbf{Number Puzzle}}
    \label{fig:galaxy}
\end{figure}     
\end{frame}
\begin{frame}{Day-1 Description}
\begin{itemize}
    \item This game is about arranging the numbers in a  puzzle of 3*4 grid
    \item  As grid consists of 12 tiles,11 numbers are arranged in 11 tiles randomly where one is kept empty.
    \item Numbers must be arranged in an ascending order in order to win the game. 
\end{itemize}   
\end{frame}
\begin{frame}{Day-2 Description}
\item Initially a frame was developed consisting of right frame and also left frames
\begin{figure}[htp]
    \centering
    \includegraphics[width=10cm]{code1.png}
    \caption{\textbf{Number Puzzle}}
    \label{fig:galaxy}
\end{figure}
\end{frame}
\begin{frame}{Day-2 result}
\begin{figure}[htp]
    \centering
    \includegraphics[width=10cm]{day 1 screenshot.png}
    \caption{\textbf{Number Puzzle}}
    \label{fig:galaxy}
\end{figure}
\end{frame}
\begin{frame}{Day-3 Description}
\item Creating buttons and an empty space
\begin{figure}[htp]
    \centering
    \includegraphics[width=10cm]{day 3 screenshot.png}
    \caption{\textbf{Number Puzzle}}
    \label{fig:galaxy}
\end{figure}
\end{frame}  
\begin{frame}{Day-3 Result}
\begin{figure}[htp]
    \centering
    \includegraphics[width=10cm]{day 3 puzzle.png}
    \caption{\textbf{Number Puzzle}}
    \label{fig:galaxy}
\end{figure}
\end{frame} 
\begin{frame}{Day-4 Result}
\begin{figure}[htp]
    \centering
    \includegraphics[width=10cm]{day 3  output screenshot.png}
    \caption{\textbf{Number Puzzle}}
    \label{fig:galaxy}
\end{figure}
\end{frame} 
\begin{frame}{Technical Stack}
\begin{enumerate}
    \item Language
    \begin{itemize}
        \item Python
    \end{itemize}
    \item Tools
    \begin{itemize}
        \item Visual Studio Code
    \end{itemize}
    \item Package Used
    \begin{itemize}
        \item TKinter
    \end{itemize}
\end{enumerate}
\end{frame}
\begin{frame}{Code stats}
\begin{itemize}
    \item  Lines of Code:146
    \item  No of functions:7
\end{itemize}
\end{frame}
\begin{frame}{Repo}
\begin{itemize}
    \item https://gitlab.com/Sowmya_G/puzzle_batch30.git 
\end{itemize}
   
\end{frame}
\begin{frame}{Challenges faced}
\begin{itemize}
    \item Had a difficulty in creating buttons.
    \item And implementing the logic is bit difficult.
    \begin{itemize}
        \item Overcoming Challenges
    \end{itemize}
\end{itemize} 
\end{frame}
\begin{frame}
\centering
     \item{\Huge{THANK YOU}}
\end{frame}
\end{document}



